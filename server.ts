import debug = require('debug');
import http = require('http');
import path = require('path');
import * as favicon from 'serve-favicon';
import * as bodyParser from 'body-parser';
import * as express from 'express';
import { Controller } from './routes/controller';
import { Request, NextFunction, Response } from 'express'

const log = debug('dyta:server')

export class Server {

	private server: http.Server
	private app: express.Application
	private viewEngine: any;

	constructor(port: number, apiHost: string, apiPort: number) {
		log('Server initialization begun')
		this.app = express();
		this.app.set('port', port);
		this.app.set('apiURI', buildAPIUri(apiHost, apiPort))
		log(`API URI is ${this.app.get('apiURI')}`)

		this.setupViewEngine();
		this.setupPipeline(port);
		this.server = this.app.listen(port)
	}

	public setupPipeline(port: number) {
		log('Pipeline setup initiated')

		this.app.use(favicon(path.join(__dirname, '../public/favicon/dyta2.ico')));
		this.app.use(express.static(path.join(__dirname, '../public')));
		this.app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
		this.app.use(bodyParser.json());

		this.configureRoutes()

		this.app.use(function errorHandler(err: any, req: Request, res: Response, next: NextFunction) {
			log('Error occured '+ err)
			switch(err) {
				case 400:
					return res.render('errors/400', { layout: false })
				case 404:
					return res.render('errors/404', { layout: false })
				case 500:
					return res.render('errors/500', { layout: false })
				default:
					res.render('errors/500', { layout: false })
			}
		});
	}

	private setupViewEngine(){
		debug('View engine setup')
		this.viewEngine = require('hbs');

		this.app.set('view engine', 'hbs');
		this.app.set('views', path.join(__dirname, '../views'));
		this.app.set('view options', { layout: 'layouts/layout' });
		this.viewEngine.registerPartials(path.join(__dirname, '../views/partials'));
	}

	private configureRoutes(){
		const ctr = new Controller(this.app.get('apiURI'));

		this.app.use(ctr.router);
	}

	public start() {
		const port = this.app.get('port')
		log('Starting to listen on port ' + port)
		this.server.listen(port)
	}
}

function buildAPIUri(apiHost: string, apiPort: number) {
	return `http://${apiHost}:${apiPort}`
}