import debug = require('debug')
import { Server } from './server'

const log = debug('dyta:startup')

type Predicate = (varValue: any) => Boolean;

function collectEnvironmentVariable(varName : any, validate: Predicate) : any {
    const evar = process.env[varName];

    log(`${varName}  ${evar}`)
    if(evar && validate(evar)){
        return evar
    }

    throw new Error(`Environment variable ${varName} is invalid or missing`);
}

const port: number = collectEnvironmentVariable('PORT', p => !isNaN(p))
const apiHost: string = collectEnvironmentVariable('API_HOST', h => h.length !== 0)
const apiPort: number = collectEnvironmentVariable('API_PORT', p => !isNaN(p))

const server: Server = new Server(port, apiHost, apiPort);
server.start();