import { 
	RequestHandler,
	NextFunction,
	Request,
	Response,
} from 'express'

export class Route {

	constructor(private _path: string){ 	}

	get path() : string{
		return this._path;
	}
	
	public getHandler(req: Request, res: Response, next: NextFunction) : any {
		next();
	}

	public postHandler(req: Request, res: Response, next: NextFunction) : any {
		next();
	}
}

export interface RenderingContext {
	title?: string, 
	scripts?: string [],
	afterBodyScripts?: string[],
	css?: string[],
	apiURI?: string,
	treeResource?: string,
}