import { Request, Response, NextFunction } from 'express'
import { Route, RenderingContext } from './route'

export class ViewRoute extends Route {

	constructor(path: string) {
		super(path);
	}

	public getHandler(req: Request, res: Response, next: NextFunction) : any {

        const ctx: RenderingContext = {
			title: 'View',
            scripts: ['alertify.min.js', 'jquery.min.js', 'bootstrap.min.js', 'view.js']
		}

		res.render('view', ctx);
	}
}