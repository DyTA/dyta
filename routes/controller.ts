import { Router, Request, Response, NextFunction, RequestHandler } from 'express';
import { Route } from './route';
import { HomepageRoute } from './home';
import { InsertRoute } from './insert';
import { VisualizeTreeRoute } from './visualizeTree';
import { VisualizeTreeDifferencesRoute } from './visualizeTreeDifferences';
import { ViewRoute } from './view';
import { TreeRoute } from './tree';
import { TimestampRoute } from './timestamp';

const homepage = '/home'

export class Controller {
	
	private _router: Router;

	constructor(apiURI: string) {
		this._router = Router();

		this._router.get('/', redirectToHomepage);
		this.addRoute(new HomepageRoute(homepage));
		this.addRoute(new ViewRoute('/view'), true);
		this.addRoute(new VisualizeTreeRoute('/view/:id/timestamp/:ts', apiURI), true);
		this.addRoute(new VisualizeTreeDifferencesRoute('/view/:id/timestamp/:ts1/:ts2', apiURI), true);
		this.addRoute(new InsertRoute('/insert', apiURI), true);
		this.addPostRoute(new TreeRoute('/tree', apiURI), true);
		this.addPostRoute(new TimestampRoute('/tree/:tid', apiURI), true);
	}

	private addRoute(route : Route, preserveThis?: boolean) {
		let handler : RequestHandler = preserveThis 
		? (req, res, next) => { route.getHandler(req, res, next) }
		: route.getHandler

		this._router.get(route.path, handler);
	}

	private addPostRoute(route : Route, preserveThis?: boolean) {
		let handler : RequestHandler = preserveThis 
		? (req, res, next) => { route.postHandler(req, res, next) }
		: route.postHandler

		this._router.post(route.path, handler);
	}

	get router() : Router {
        return this._router;
    }
}

function redirectToHomepage( req: Request, res: Response, next: NextFunction, ) : any {
	res.redirect(homepage);
}