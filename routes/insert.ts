import { Request, Response, NextFunction } from 'express'
import { Route, RenderingContext } from './route'

export class InsertRoute extends Route {

	constructor(path: string, private apiURI: string) {
		super(path);
	}

	public getHandler(req: Request, res: Response, next: NextFunction) : any {
		
		const ctx: RenderingContext = {
			apiURI: this.apiURI,
			title: 'Insert',
			scripts: ['insertTrees.js', 'jquery.min.js']
		}

		res.render('insert', ctx);
	}

}