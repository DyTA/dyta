import { Request, Response, NextFunction } from 'express'
import { Route, RenderingContext } from './route'
import { RequestResponse } from "request";
import proxy = require('request')
import debug = require('debug')

const log = debug('dyta:timestamp');

export class TimestampRoute extends Route {

	constructor(path: string, private apiURI: string) {
		super(path);
	}

	public postHandler(req: Request, res: Response, next: NextFunction) : any {
		const tid: number = req.params['tid']
		const apiEndpoint: string = this.apiURI + '/tree/' + tid
		const opts = { 
			uri: apiEndpoint,
			json: req.body,
		}

		proxy.post(opts, (error: any, response: RequestResponse, body: any) => {
			if(error) {
				log(error)
				return next(500)
			}

			if(response.statusCode === 201){
				log('API successfully saved the changes and created a new timestamp ts='+body.ts)
				res.status(201).json(body);
				return;
			}

			next(500);
		})
	}

}