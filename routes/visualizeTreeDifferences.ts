import { Request, Response, NextFunction } from 'express'
import { Route, RenderingContext } from './route'
import { RequestResponse } from "request";
import { Slider } from "./slider";
import request = require('request')
import debug = require('debug')

const log = debug('dyta:differences')
const visualizationTable = require('../../visualization-table.json')

export class VisualizeTreeDifferencesRoute extends Route {

	constructor(path: string, private apiURI: string) {
		super(path);
	}

	public getHandler(req: Request, res: Response, next: NextFunction) : any {
		const treeID = req.params['id']
		let tsA = req.params['ts1']
		let tsB = req.params['ts2']
		log(`Inbound request for differences in tree ${treeID} between timestamps ${tsA} and ${tsB}`)

		if(isNaN(treeID) || isNaN(tsA) || isNaN(tsB) || tsA > tsB || tsA < 0) {
			return next(400)
		}

		request.get(`${this.apiURI}/tree/${treeID}/info`, (error: any, response: RequestResponse, body: any) => {
			if(error || !response || response.statusCode !== 200){
				log(error)
				return next(500)
			}

			body = JSON.parse(body);
			tsA = parseInt(tsA)
			tsB = parseInt(tsB)

			const type: number = body.type
			const maxTs: number = parseInt(body.ts)
			
			if(tsB > maxTs) {
				return next(400);
			}

			let script: string = visualizationTable[type] || visualizationTable[1]
			let context : TwoTreesVisualizationContext = {
				apiURI: this.apiURI, 
				afterBodyScripts: ['twoTreesDisplay.js', 'palette.js', script], 
				scripts: ['d3.v3.min.js', 'alertify.min.js', 'jquery.min.js', 'bootstrap.min.js', 'export-newick.js'],
				css: ['nodes.css', 'alertify.min.css', 'visualize.css'],
				treeID,
				tsA, 
				tsB,
				name: body.name,
        		description: body.description,
				type,
				dendrogram: type == 1,
				fdl: type == 2,
			};

			context.slider = buildSlider(context, tsA, tsB, maxTs)

			res.render('visualizeTwoTrees', context);
		})
	}
}

function buildSlider(ctx: TwoTreesVisualizationContext, tsA: number, tsB: number, maxTs: number) : Slider | undefined{
	if(maxTs < 1) 
		return

	ctx.scripts!.push('bootstrap-slider.min.js')
	ctx.css!.push('bootstrap-slider.min.css');
	
	const ticks = []
	for(let ts = 0; ts <= maxTs; ts++) {
		ticks.push(ts)
	}

	return {
		ticks: JSON.stringify(ticks),
		values: JSON.stringify([tsA, tsB]),
		value: tsB,
		min: 0,
		max: maxTs
	}
}

interface TwoTreesVisualizationContext extends RenderingContext {
	type?: number,
	dendrogram: boolean,
	fdl: boolean,
	name: string,
	description: string,
	treeID: number,
	tsA: number, tsB: number
	slider?: Slider
}