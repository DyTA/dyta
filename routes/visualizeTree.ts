import { Request, Response, NextFunction } from 'express'
import { Route, RenderingContext } from './route'
import { RequestResponse } from "request";
import { Slider } from "./slider";
import request = require('request')

const visualizationTable = require('../../visualization-table.json')
const MAX: string = 'max'

export class VisualizeTreeRoute extends Route {

	constructor(path: string, private apiURI: string) {
		super(path);
	}

	public getHandler(req: Request, res: Response, next: NextFunction) : any {
		const treeID = req.params['id']
		const timestamp = req.params['ts']
		
		if(isNaN(treeID) ||  isInvalidTimestamp(timestamp)) {
			return next(400)
		}

		this.renderTree(res, next, treeID, timestamp)
	}

	private renderTree(res: Response, next: NextFunction, treeID: number, timestamp: number | 'max') {
		request.get(`${this.apiURI}/tree/${treeID}/info`, (error: any, response: RequestResponse, body: any) => {
				if(error || !response) {
					return next(500)
				}

				if(response.statusCode === 404) {
					return next(404)
				}

				body = JSON.parse(body);

				const maxTs: number = parseInt(body.ts)
				if(timestamp === 'max') 
					timestamp = maxTs;
				
				if(timestamp > maxTs) {
					return next(404)
				}

				const type: number = body.type
				let script: string = visualizationTable[type] || visualizationTable[1]

				let context : VisualizationContext = {
					afterBodyScripts: ['display.js', 'palette.js', script], 
					scripts: ['d3.v3.min.js', 'alertify.min.js', 'jquery.min.js', 'bootstrap.min.js', 'export-newick.js', 'treeFormsManager.js'],
					css: ['nodes.css', 'alertify.min.css', 'visualize.css'],
					treeResource: this.apiURI + `/tree/${treeID}/timestamp/${timestamp}`,
					apiURI: this.apiURI,
					name: body.name,
					description: body.description,
					ts: timestamp,
					type,
					treeID,
					dendrogram: type == 1,
					fdl: type == 2,
					changeEndpoint: '/tree/'+treeID,
					renderForms: timestamp == maxTs
				};

				context.slider = buildSlider(context, timestamp, maxTs)

				res.render('visualizeTree', context);
		})
	}
}

function isInvalidTimestamp(ts: number | string) {
	if(ts == MAX) {
		return false;
	}

	return isNaN(ts as number)
}



function buildSlider(ctx: VisualizationContext, ts: number, maxTs: number) : Slider | undefined{
	if(maxTs < 1) 
		return

	ctx.scripts!.push('bootstrap-slider.min.js')
	ctx.css!.push('bootstrap-slider.min.css');
	
	const ticks = []
	for(let ts = 0; ts <= maxTs; ts++) {
		ticks.push(ts)
	}

	return {
		ticks: JSON.stringify(ticks),
		value: ts,
		min: 0,
		max: maxTs
	}
}

interface VisualizationContext extends RenderingContext {
	type?: number,
	dendrogram: boolean,
	treeID: number,
	fdl: boolean,
	ts: number,
	name: string,
	description: string,
	changeEndpoint: string,
	renderForms: boolean,
	slider?: Slider
}