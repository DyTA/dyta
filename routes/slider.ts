export type Slider = {
	ticks: string 
	value?: number
	values?: string
	min: number
	max: number
}