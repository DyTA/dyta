import { Request, Response, NextFunction } from 'express'
import { Route } from './route'

export class HomepageRoute extends Route {

	constructor(path: string) {
		super(path);
	}

	public getHandler(req: Request, res: Response, next: NextFunction) : any {
		
		res.render('homepage', { layout: false });
	}

}