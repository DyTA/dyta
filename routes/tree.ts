import { Request, Response, NextFunction } from 'express'
import { Route, RenderingContext } from './route'
import { RequestResponse } from "request";
import proxy = require('request')
import debug = require('debug')

const log = debug('dyta:tree');

export class TreeRoute extends Route {

	constructor(path: string, private apiURI: string) {
		super(path);
	}

	public postHandler(req: Request, res: Response, next: NextFunction) : any {
		const apiEndpoint: string = this.apiURI + '/tree'
		const opts = { 
			uri: apiEndpoint,
			json: req.body,
		}

		proxy.post(opts, (error: any, response: RequestResponse, body: any) => {
			if(error) {
				return next(500);
			}

			log('API responded to proxy request with status '+response.statusCode)
			if(response.statusCode === 201){
				const treeID: number = response.body.id;
				const ti = 0;
				res.redirect(`/view/${treeID}/timestamp/${ti}`)
				return;
			}

			next(response.statusCode)
		})
	}

}