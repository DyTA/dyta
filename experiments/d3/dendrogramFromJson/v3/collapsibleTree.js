// Set the colors used on nodes
const NODE_OLD_COLLAPSIBLE_FILL_COLOR = "#f0f0f0"
const NODE_OLD_FILL_COLOR = "#c7c8c7"
const NODE_OLD_STROKE_COLOR = "#7e7e7e"
const NODE_NEW_COLLAPSIBLE_FILL_COLOR = "#5bf178"
const NODE_NEW_FILL_COLOR = "#48c560"
const NODE_NEW_STROKE_COLOR = "#41b257"

// Set the colors used on links
const LINK_OLD_STROKE_COLOR = "#7e7e7e"
const LINK_NEW_STROKE_COLOR = "#41b257"

const OLD_NODE_STATE = "old"

const TRANSITION_DURATION = 450

const LEAF_HEIGHT = 22
const CIRCLE_RADIUS = 6
const TEXT_DISTANCE_FROM_LEAF = 13
const NODE_TEXT_SIZE = 12
const NODE_TEXT_FONT = "sans-serif"
const DENDROGRAM_GROUP_ID = "dendrogram-g-id"

/* This information comes from the tree data 
 * For different tree datas the info might change, 
 * so by changing these consts value its possible to define your own field names :) */
const NODE_Y_FIELD_NAME = "width"
const TREE_NUMBER_OF_LEAF_NODES_FIELD_NAME = "numberOfLeaves"
const TREE_MAX_LENGTH_FIELD_NAME = "treeMaxLength"

// Misc needed outside of d3.json scope
let i = 0,
    branchLengthRatio,
    treeWidth,
    treeHeight,
    svgGroup,
    tree,
    root;

// size of the diagram
let svgWidth = window.outerWidth - 100;
let svgHeight = window.outerHeight - 150;

let zoomListener = d3.behavior.zoom()
    .on("zoom", defaultZoom);

d3.json("rootedLeafOnly.json", function (error, treeFromJson) {

    // define the baseSvg, attaching a class for styling and the zoomListener
    let svg = d3.select("svg")
        .attr("width", svgWidth)
        .attr("height", svgHeight)
        .attr("class", "overlay")
        .call(zoomListener)
        .on("dblclick.zoom", null);

    // Append a group which holds all nodes and which the zoom Listener can act upon.
    svgGroup = svg.append("g");

    treeWidth = svgWidth - 100
    treeHeight = treeFromJson[TREE_NUMBER_OF_LEAF_NODES_FIELD_NAME] * LEAF_HEIGHT
    tree = d3.layout.cluster()
        .size([treeHeight, treeWidth]);

    branchLengthRatio = treeWidth / treeFromJson[TREE_MAX_LENGTH_FIELD_NAME];

    // Define the root
    root = treeFromJson;

    update(root);
    centerTree(treeWidth, treeHeight, svgWidth, svgHeight, zoomListener)
});

function update(source) {
    // Compute the new tree layout.
    let nodes = tree.nodes(root).reverse(),
        links = tree.links(nodes);

    // Set widths between levels based on maxLabelLength.
    nodes.forEach(function (d) {
        d.y = d[NODE_Y_FIELD_NAME] * branchLengthRatio
    });

    updateNodes(source, nodes);
    updateLinks(source, links);

    // Stash the old positions for transition.
    nodes.forEach(function (d) {
        d.x0 = d.x;
        d.y0 = d.y;
    });
}

function updateNodes(source, nodes) {
    // Update the nodes…
    let node = svgGroup.selectAll("g.node")
        .data(nodes, function (d) {
            return d.id || (d.id = ++i);
        });

    // Enter any new nodes at the parent's previous position.
    let nodeEnter = node.enter().append("g")
        .attr("class", "node")
        .attr("transform", function (d) {
            return "translate(" + source.y0 + "," + source.x0 + ")";
        })
        .on('click', click);

    // Add Circle for the nodes
    nodeEnter.append('circle')
        .attr('class', 'node')
        .attr('r', CIRCLE_RADIUS)
        .style("fill", function (d) {
            return setNodeFillColor(d)
        })
        .style("stroke", function (d) {
            return setNodeStrokeColor(d)
        });

    // Show vertex id when hovering
    nodeEnter.append("svg:title")
        .text(function (d) {
            return "Vertex id: " + d.id;
        });

    // Add labels for the nodes
    nodeEnter.append('text')
        .attr("dy", ".35em")
        .attr("x", function (d) {
            return TEXT_DISTANCE_FROM_LEAF;
        })
        .attr("text-anchor", function (d) {
            return "start";
        })
        .attr("font", function (d) {
            return NODE_TEXT_SIZE + "px " + NODE_TEXT_FONT;
        })
        .text(function (d) { return d.name; });

    // Transition nodes to their new position.
    let nodeUpdate = node.transition()
        .duration(TRANSITION_DURATION)
        .attr("transform", function (d) {
            return "translate(" + d.y + "," + d.x + ")";
        });

    // Without these two updates if you fast 
    // double click in a node
    // the node and the text disappear
    nodeUpdate.select('circle.node')
        .attr('r', CIRCLE_RADIUS)
        .style("fill", function (d) {
            return setNodeFillColor(d)
        })
        .style("stroke", function (d) {
            return setNodeStrokeColor(d)
        })
        .attr('cursor', 'pointer');
    nodeUpdate.select("text")
        .style("fill-opacity", 1);

    // Remove any exiting nodes
    let nodeExit = node.exit().transition()
        .duration(TRANSITION_DURATION)
        .attr("transform", function (d) {
            return "translate(" + source.y + "," + source.x + ")";
        })
        .remove();

    // On exit reduce the node circles size to 0
    nodeExit.select('circle')
        .attr('r', 1e-6);

    // On exit reduce the opacity of text labels
    nodeExit.select('text')
        .style('fill-opacity', 1e-6);
}

function updateLinks(source, links) {
    // Update the links…
    let link = svgGroup.selectAll("path.link")
        .data(links, function (d) {
            return d.target.id;
        });

    // Enter any new links at the parent's previous position.
    link.enter().insert("path", "g")
        .attr("class", "link")
        .style("stroke", function (d) {
            return setLinkStrokeColor(d)
        })
        .attr('d', function (d) {
            return elbow(d)
        });

    // Transition links to their new position.
    link.transition()
        .duration(TRANSITION_DURATION)
        .attr('d', function (d) {
            return elbow(d)
        })

    // Transition exiting nodes to the parent's new position.
    link.exit().transition()
        .duration(TRANSITION_DURATION)
        .style("stroke", function (d) {
            return setLinkStrokeColor(d)
        })
        .attr('d', function (d) {
            return exitElbow(d)
        })
        .remove();
}

function collapse(d) {
    if (d.children) {
        d._children = d.children;
        d._children.forEach(collapse);
        d.children = null;
    }
}

function expand(d) {
    if (d._children) {
        d.children = d._children;
        d.children.forEach(expand);
        d._children = null;
    }
}

function setNodeFillColor(d) {
    if (d.state === OLD_NODE_STATE)
        return d._children ? NODE_OLD_COLLAPSIBLE_FILL_COLOR : NODE_OLD_FILL_COLOR;
    else
        return d._children ? NODE_NEW_COLLAPSIBLE_FILL_COLOR : NODE_NEW_FILL_COLOR;
}

function setNodeStrokeColor(d) {
    return d.state === OLD_NODE_STATE ? NODE_OLD_STROKE_COLOR : NODE_NEW_STROKE_COLOR;
}

function setLinkStrokeColor(d) {
    return d.target.state === OLD_NODE_STATE ? LINK_OLD_STROKE_COLOR : LINK_NEW_STROKE_COLOR;
}

// Function to center visualization on node
function centerOnNode(source, zoomListener) {
    let scale = zoomListener.scale();
    let x = source.y0 * scale;
    let y = source.x0 * scale;
    let translateX = svgWidth / 2 - x;
    let translateY = svgHeight / 2 - y;
    svgGroup.transition()
        .duration(TRANSITION_DURATION)
        .attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
    zoomListener.scale(scale);
    zoomListener.translate([translateX, translateY]);
}

// Function to center tree on svg
function centerTree(treeWidth, treeHeight, svgWidth, svgHeight, zoomListener) {
    let scale = zoomListener.scale();
    let translateX = (svgWidth / 2 - treeWidth / 2 * scale)
    let translateY = (svgHeight / 2 - treeHeight / 2 * scale)
    svgGroup.transition()
        .duration(TRANSITION_DURATION)
        .attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
    zoomListener.scale(scale);
    zoomListener.translate([translateX, translateY]);
}

// Toggle children function
function collapseOrExpand(d) {
    if (d.children) {
        d._children = d.children
        d._children.forEach(collapse)
        d.children = null
    } else if (d._children) {
        d.children = d._children
        d.children.forEach(expand)
        d._children = null
    }
    return d;
}

// Toggle children on click.
function click(d) {
    if (d3.event.defaultPrevented) return; // click suppressed
    d = collapseOrExpand(d);
    update(d);
    centerOnNode(d, zoomListener);
}

function defaultZoom() {
    svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
}

// Creates a elbow like path from parent to child node
function elbow(d) {
    return "M" + d.source.y + "," + d.source.x //tanto "," como " " sao aceites para separar a coordenada x do y
        + "V" + d.target.x
        + "H" + d.target.y;
}

// Used in exit transition of links
function exitElbow(d) {
    return "M" + d.source.y + "," + d.source.x
        + "V" + d.source.x
        + "H" + d.source.y;
}