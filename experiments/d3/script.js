'use strict';
// Assuming data already comes in a convenient json format that d3 is expecting
// TODO: represent same thing but with different format

const viewportWidth = document.documentElement.clientWidth * 0.99
const viewportHeight = document.documentElement.clientHeight * 0.9
const nodeRadius = 5
const dataSrc = 'data.json'

let adjustedWidth = viewportWidth - nodeRadius * 2
let adjustedHeight = viewportHeight - nodeRadius * 2
let svg = d3.select('body')
    .append('svg')
    .attr('width', viewportWidth)
    .attr('height', viewportHeight)
    .attr('transform', `translate(0, ${nodeRadius})`)

let treeLayout = d3.tree()
    .size([adjustedWidth, adjustedHeight])

d3.json(dataSrc, onArrival) // fetch data

function onArrival(rawData){
    // if raw data is not in an hierarchical format then must transform it by using d3.stratify()
    // it is possible to pass a function as the second argument to specify how to retrieve a nodes children
    // if unspecified node.children is assumed
    let root = d3.hierarchy(rawData)

    let nodes = treeLayout(root).descendants()
    formatNodes(nodes)
    
    let links = nodes.slice(1) // skip root
    formatLinks(links)    
}

function formatNodes(nodes){
    // bind data to visual elements that are created on demand and its characteristics specified with a fluent syntax
    let node = svg.selectAll('.node')
        .data(nodes)
        .enter();

    node.append('circle')
        .attr('r', nodeRadius)
        .attr('class', 'node')
        .attr('transform', (n) => `translate(${n.x}, ${n.y})`)

    node.append("text")
        .attr("x", n => n.x)
        .attr("y", n => n.y)
        .attr("dx", nodeRadius) 
        .attr("dy", nodeRadius)
        .text((n) => n.data.name)
}

function formatLinks(links){
    svg.selectAll('.link')
        .data(links)
        .enter()
        .append('path')
        .attr('class', 'link')
        .attr('d', makePathToParent)
}

function makePathToParent(node) {
    let parent = node.parent

    return `M ${node.x} ${node.y} 
            V ${parent.y}
            H ${parent.x}`
}