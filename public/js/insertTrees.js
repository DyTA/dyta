function suggestDendrogram(){
    $('input[name="treetype"]').attr('checked', false);
    $("#treetype1").attr("checked", "checked");
    $("#treesource").val("(((((0:1.5,3:1.5):0.6875,(((6:0.5,8:0.5):0.25,7:0.75):0.41666663,9:1.1666666):1.0208334):0.14583325,(4:1.0,5:1.0):1.3333333):0.16666675,1:2.5):0.055555582,2:2.5555556);")
    $("#tree-name").val("Suggested dendrogram tree")
    $("#tree-description").val("This is the suggested tree for the dendrogram visualization")
}

function suggestGraphlike(){
    $('input[name="treetype"]').attr('checked', false);
    $("#treetype2").attr("checked", "checked");
    $("#treesource").val("((1:3)4:4,8:2,(5:2)6:4,2:4,3:5)7;")
    $("#tree-name").val("Suggested graph-like tree")
    $("#tree-description").val("This is the suggested tree for the graph-like visualization")
}