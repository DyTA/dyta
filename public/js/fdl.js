const NODE_RADIUS = 8
const NODE_TEXT_SIZE = 12
const NODE_TEXT_FONT = "ubuntu, verdana, arial"

const CENTER_TRANSITION_DURATION = 850
const LINK_DISTANCE_FACTOR = 20

class Visualization {
	constructor() {
		this.i = 0
		this.inputBranchLengthRatio = 1
		this.treeWidth
		this.treeHeight
		this.colorPalette = ["#181818", "#d4d4d4"]
		this.svgGroup
		this.tree
		this.node
		this.link
		this.roots = []
		this.svgWidth
		this.svgHeight
		let thisContext = this
		this.zoomListener = d3.behavior.zoom()
			.on("zoom", function () {
				return thisContext.defaultZoom.call(thisContext);
			})
	}

	initVisualization(treesFromJson, svg, colorPalette) {
		this.svgWidth = parseInt(svg.style("width"))
		this.svgHeight = parseInt(svg.style("height"))

		if (colorPalette)
			this.colorPalette = colorPalette

		let trees = treesFromJson.constructor == Array ? treesFromJson : [treesFromJson]

		let nrOfNodes = 0
		trees.forEach((tree) => nrOfNodes += tree.root.nChildren + 1)
		let maxLength = 0
		trees.forEach((tree) => maxLength = maxLength < tree[TREE_MAX_LENGTH_FIELD_NAME] ?
			tree[TREE_MAX_LENGTH_FIELD_NAME] : maxLength)

		this.treeWidth = nrOfNodes * maxLength * LINK_DISTANCE_FACTOR
		this.treeHeight = this.treeWidth

		let thisContext = this
		this.tree = d3.layout.force()
			.linkDistance(function (link) {
				return link.target[BRANCH_LENGTH_NAME_FIELD] * LINK_DISTANCE_FACTOR * thisContext.inputBranchLengthRatio
			})
			.charge(-500)
			.gravity(0.1)
			.friction(0.9)
			.size([this.treeWidth, this.treeHeight])
			.on("tick", function () {
				return thisContext.tick.call(thisContext)
			});

		svg.attr("class", "overlay")

		//Invisible layer to receive zoom input
		let rect = svg.append("rect")
			.attr("x", 0)
			.attr("y", 0)
			.attr("width", this.svgWidth)
			.attr("height", this.svgHeight)
			.attr("fill", "transparent")
			.call(this.zoomListener)
			.on("dblclick.zoom", null);

		this.svgGroup = svg.append("g");

		trees.forEach((tree) => {
			this.roots.push(tree.root);
			tree.root.x = this.treeWidth / 2
			tree.root.y = this.treeHeight / 2
			if(tree.root.children)
				tree.root.children.forEach((child) => collapse(child))
		})

		//this.root.fixed = true
		this.update();

		centerSvgOnNode(this.roots[0], this.svgWidth, this.svgHeight, this.zoomListener, this.svgGroup);
	}

	update() {
		let nodes = flattenTrees(this.roots),
			links = d3.layout.tree().links(nodes);

		// Update the force layout
		this.tree
			.nodes(nodes)
			.links(links)
			.start();

		this.updateNodes(nodes)
		this.updateLinks(links)
	}

	updateNodes(nodes) {
		this.node = this.svgGroup.selectAll(".node")
			.data(nodes, function (d) {
				return d[NODE_ID_NAME_FIELD];
			});

		let thisContext = this
		let nodeEnter = this.node.enter().append("g")
			.attr("class", "node")
			.attr("cursor", "pointer")
			.on("click", function (d) {
				return thisContext.click.call(thisContext, d)
			})
			.call(this.tree.drag);

		nodeEnter.append("circle")
			.attr("r", NODE_RADIUS)
			.style("fill", function (d) {
				return thisContext.setNodeFillColor.call(thisContext, d);
			})
			.style("stroke", function (d) {
				return thisContext.setNodeStrokeColor.call(thisContext, d);
			});

		nodeEnter.append("svg:title")
			.text(function (d) {
				return "Vertex id: " + d[NODE_ID_NAME_FIELD];
			});

		nodeEnter.append("text")
			.attr('class', 'name')
			.attr("dx", "-4px")
			.attr("dy", "1.7em")
			.style("font-size", NODE_TEXT_SIZE)
			.style("font-family", NODE_TEXT_FONT)
			.attr("fill", "black")
			.text(d => d[NODE_NAME_FIELD]);

		nodeEnter.append("text")
			.attr('class', 'children')
			.attr("dx", "-4px")
			.attr("dy", "4px")
			.style("font-size", NODE_TEXT_SIZE)
			.style("font-family", NODE_TEXT_FONT)
			.attr("fill", "black")
			.text(this.setNodeText);

		this.node.transition()
			.select("circle")
			.style("fill", function (d) {
				return thisContext.setNodeFillColor.call(thisContext, d);
			})
			.style("stroke", function (d) {
				return thisContext.setNodeStrokeColor.call(thisContext, d);
			})

		this.node.transition()
			.select("text.children")
			.text(this.setNodeText)

		this.node.exit()
			.remove();
	}

	updateLinks(links) {
		let thisContext = this

		this.link = this.svgGroup.selectAll(".link")
			.data(links, function (d) {
				return d.target.id;
			})

		this.link.enter()
			.insert("line", ".node")
			.attr("class", "link")
			.style("stroke", function (d) {
				return thisContext.setLinkStrokeColor.call(thisContext, d);
			})
			.style("stroke-dasharray", thisContext.setDash)
			.append("svg:title")
			.text(function (link) {
				return "Branch length: " + link.target[BRANCH_LENGTH_NAME_FIELD]
			});

		this.link.exit()
			.remove();
	}

	changeColorPalette(colorPalette) {
		let thisContext = this
		this.colorPalette = colorPalette

		//Recolor circles
		this.svgGroup.selectAll("circle")
			.style("fill", function (d) {
				let color = thisContext.setNodeFillColor.call(thisContext, d);
				return color
			})
			.style("stroke", function (d) {
				return thisContext.setNodeStrokeColor.call(thisContext, d);
			})

		//Recolor links
		this.svgGroup.selectAll(".link").
			style("stroke", function (d) {
				return thisContext.setLinkStrokeColor.call(thisContext, d);
			})
	}

	// Function called on click event
	click(d) {
		if (d3.event.defaultPrevented) return; // ignore drag
		if (d3.event.ctrlKey)
			d = collapseOrExpand(d);
		else
			d = collapseOrSingleExpand(d)
		this.update();
		centerSvgOnNode(d, this.svgWidth, this.svgHeight, this.zoomListener, this.svgGroup);
	}

	tick() {
		this.link
			.attr("x1", function (d) {
				return d.source.x;
			})
			.attr("y1", function (d) {
				return d.source.y;
			})
			.attr("x2", function (d) {
				return d.target.x;
			})
			.attr("y2", function (d) {
				return d.target.y;
			});

		this.node.attr("transform", function (d) {
			return "translate(" + d.x + "," + d.y + ")";
		});
	}

	/**
	 * Function that searches for a node with a given name and 
	 * and expands the path to it
	 * @param nodeName the name of the node we are searching for
	 */
	searchNodeAndExpandThePathToIt(nodeName) {
		let nodes

		for (let i = 0; !nodes && i < this.roots.length; i++)
			nodes = this.searchNodesToExpandFrom(this.roots[i], nodeName)

		if (nodes) {
			//idx = 1 in order not to expand the searched node, that is always at idx = 0
			for (let idx = 1; idx < nodes.length; idx++)
				singleLevelExpand(nodes[idx])

			this.update()
			centerSvgOnNode(nodes[0], this.svgWidth, this.svgHeight, this.zoomListener, this.svgGroup);
		} else {
			alertify.error("The desired node couldn't be found in this tree :(")
		}
	}

	/**
	 * Function that searches for a node with a given name and 
	 * returns an array with the nodes that links the root to 
	 * the node or null if it didn't find the desired node
	 * The array has the searched node in first index and
	 * the other nodes on the rest of the indexes
	 * @param currentNode the current node we are searching on
	 * @param nodeName the name of the node we are searching for
	 * @return the array with the pathway nodes or null
	 */
	searchNodesToExpandFrom(currentNode, nodeName) {
		let nodesArray = []
		if (this.internalSearchNodesToExpandFrom(currentNode, nodeName, nodesArray))
			return nodesArray;
		return null;
	}

	/**
	 * Internal function that searches for a node with a given name
	 * saving the nodes it passed through on nodesArray parameter
	 * @param currentNode the current node we are searching on
	 * @param nodeName the name of the node we are searching for
	 * @param nodesArray the array with the pathway nodes
	 * @return true or false whether it found the node or not
	 */
	internalSearchNodesToExpandFrom(currentNode, nodeName, nodesArray) {
		if (currentNode[NODE_NAME_FIELD] == nodeName) {
			nodesArray.push(currentNode)
			return true;
		}
		if (currentNode._children) {
			for (let i = 0; i < currentNode._children.length; i++) {
				if (this.internalSearchNodesToExpandFrom(currentNode._children[i], nodeName, nodesArray)) {
					nodesArray.push(currentNode)
					return true
				}
			}
		}
		if (currentNode.children) {
			for (let i = 0; i < currentNode.children.length; i++)
				if (this.internalSearchNodesToExpandFrom(currentNode.children[i], nodeName, nodesArray)) {
					nodesArray.push(currentNode)
					return true
				}
		}
		return false;
	}

	//Function called on zoom event
	defaultZoom() {
		this.svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
	}

	changeBranchLengthRatio(increment) {
		this.inputBranchLengthRatio += increment
		this.update()
	}

	setNodeFillColor(d) {
		if (!isNaN(d.state))
			return isCollapsible(d) ? this.colorPalette[d.state * 2 + 1] : this.colorPalette[d.state * 2];
		else
			return isCollapsible(d) ? this.colorPalette[1] : this.colorPalette[0];
	}

	setNodeStrokeColor(d) {
		return !isNaN(d.state) ? this.colorPalette[d.state * 2] : this.colorPalette[0];
	}

	setLinkStrokeColor(d) {
		return !isNaN(d.target.linkstate) ? this.colorPalette[d.target.linkstate * 2] : this.colorPalette[0];
	}

	setNodeText(d) {
		return (d._children && d._children.length > 0) ? d[NODE_NUMBER_OF_CHILDREN_FIELD_NAME] : "";
	}

	setDash(link) {
        return link.target.removed ? ("3, 3") : ""
    }
}

// Returns a list of all descendants of all roots
function flattenTrees(roots) {
	let nodes = []

	function recursiveFlatten(node) {
		if (node.children)
			node.children.forEach(recursiveFlatten);

		nodes.push(node);
	}

	roots.forEach(recursiveFlatten);

	return nodes;
}

function isCollapsible(node) {
	return node._children && (node._children.length > 0)
}

// Function to center visualization on node
function centerSvgOnNode(source, svgWidth, svgHeight, zoomListener, svg) {
	let scale = zoomListener.scale();
	let x = source.x * scale;
	let y = source.y * scale;
	let translateX = svgWidth / 2 - x;
	let translateY = svgHeight / 2 - y;
	svg.transition()
		.duration(CENTER_TRANSITION_DURATION)
		.attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
	zoomListener.scale(scale);
	zoomListener.translate([translateX, translateY]);
}

function collapseOrExpand(d) {
	if (d.children) {
		d._children = d.children
		d._children.forEach(collapse)
		d.children = null
	} else if (d._children) {
		d.children = d._children
		d.children.forEach(expand)
		d._children = null
	}
	return d;
}

function collapseOrSingleExpand(d) {
	if (d.children) {
		d._children = d.children
		d._children.forEach(collapse)
		d.children = null
	} else if (d._children) {
		d.children = d._children
		d._children = null
	}
	return d;
}

function collapse(d) {
	if (d.children) {
		d._children = d.children;
		d._children.forEach(collapse);
		d.children = null;
	}
}

function expand(d) {
	if (d._children) {
		d.children = d._children;
		d.children.forEach(expand);
		d._children = null;
	}
}

function singleLevelExpand(d) {
	if (d._children) {
		d.children = d._children;
		d._children = null;
	}
}
