function exportNewick(treeResource) {
	const headers = new Headers({
		'accept': 'application/newick'
	});

	const req = {
		method: 'GET',
		headers: headers,
		mode: 'cors',
		cache: 'default'
	};

	fetch(treeResource, req)
	.then(response => {
		if(response.ok) {
			return response.text();
		}

		throw new Error('Failure to fetch newick representation')
	})
	.then((nwk) => {
		document.getElementById('newick-export-text').textContent = nwk;
		$('#newick-export').modal('show');
	})
	.catch((reason) => {
		alertify.error("Couldn't export your tree");
	})
}

function copyToClipboard() {
	window.getSelection().removeAllRanges();

	const range = document.createRange();
	range.selectNode(document.getElementById('newick-export-text'));
	window.getSelection().addRange(range);
	
	document.execCommand('copy');

	window.getSelection().removeAllRanges();
}

function setExportResource(tree) {
	document.getElementById('nwk-export-btn').onclick = () => exportNewick(tree)
}