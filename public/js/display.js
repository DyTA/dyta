/* This information comes from the json tree data 
 * For different tree datas the info might change, 
 * so by changing these consts value its possible to define your own field names :) */
const NODE_DEPTH_FIELD_NAME = "distanceToRoot"
const TREE_NUMBER_OF_LEAF_NODES_FIELD_NAME = "numberOfLeaves"
const TREE_MAX_LENGTH_FIELD_NAME = "longestDistance"
const NODE_NUMBER_OF_CHILDREN_FIELD_NAME = "nChildren"
const NODE_NAME_FIELD = 'name'
const NODE_ID_NAME_FIELD = 'id'
const BRANCH_LENGTH_NAME_FIELD = 'branch_length'

let currentVisualization;
let apiHost;
let treeID;
let cachedTrees = [];

function loadTree(treeResource) {
    $('#loading-screen').modal('show');
    let start = performance.now();
    d3.json(treeResource, function (error, tree) {
        let end = performance.now();
        console.log('server took ', end - start, ' ms to respond');
        $('#loading-screen').modal('hide');
        if (error) {
            alertify.error('Could not load your tree. Try refreshing the page.')
            return;
        }

        let svgHeight = window.outerHeight * 0.8;

        let svg = d3.select("#svg1")
            .attr("width", "100%")
            .attr("height", svgHeight)

        svg.selectAll('*').remove();
        currentVisualization = new Visualization();
        currentVisualization.initVisualization(tree, svg);
    });
}

function onSearchNodeClick() {
    currentVisualization.searchNodeAndExpandThePathToIt(document.getElementById('search-node-input').value)
}

function changeBranchLengthRatio(_, increment){
    currentVisualization.changeBranchLengthRatio(increment)
}

function changeBranchHeightRatio(_, increment){
    currentVisualization.changeBranchHeightRatio(increment)
}

function onPageLoad(apiUri, _treeID, ts) {
    apiHost = apiUri;
    treeID = _treeID;
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
    buildSlidingWindow(ts)
    loadTree(buildTreeUri(ts));
}

function buildTreeUri(ts) {
     return apiHost + '/tree/' + treeID + '/timestamp/' + ts
}

function buildTreesDiffUri(tsA, tsB) {
    return apiHost + '/tree/' + treeID + '/diff?ta=' + tsA + '&tb=' +tsB
}

function buildSlidingWindow(ts) {
    if(typeof Slider === "undefined")
        return

    slidingWindow = new Slider('#sliding-window', {
        tooltip_position: "bottom"
    })

    slidingWindow.hasChanged = function(now) {
        const old = this.oldValue
        this.oldValue = now;

        return old !== now;
    }

    slidingWindow.on('slideStop', onSlideStop)
}

function onSlideStop(mEvt) {
    if(!slidingWindow.hasChanged(mEvt)) {
        return false;
    }
    
    const ts = slidingWindow.getValue()
    const tree = buildTreeUri(ts)

    setExportResource(tree)    
    loadTree(tree)

    return false;
}