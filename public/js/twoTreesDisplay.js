/* This information comes from the json tree data 
 * For different tree datas the info might change, 
 * so by changing these consts value its possible to define your own field names :) */
const NODE_DEPTH_FIELD_NAME = "distanceToRoot"
const TREE_NUMBER_OF_LEAF_NODES_FIELD_NAME = "numberOfLeaves"
const TREE_MAX_LENGTH_FIELD_NAME = "longestDistance"
const NODE_NUMBER_OF_CHILDREN_FIELD_NAME = "nChildren"
const NODE_NAME_FIELD = 'name'
const NODE_ID_NAME_FIELD = 'id'
const BRANCH_LENGTH_NAME_FIELD = 'branch_length'
const DENDROGRAM_TREE_TYPE = 1

const cbfPaletteName = "tol-rainbow"
const normalPaletteName = "tol-sq"
const palettes = [normalPaletteName, cbfPaletteName]

let apiHost;
let treeID;
let trees = [];
let visualizations = [];
let currentTreeDifferences;
let numberOfDifferences = 2;
let slidingPairWindow,
    slidingWindow;
let isSlidingWindow = false;
let currentDifferencesSvg;
let currentPaletteName = normalPaletteName;
let currentPalette;
let legendData;

function loadTrees(firstTreeResource, secondTreeResource, treeDifferencesResource) {
    $('#loading-screen').modal('show');
    let start = performance.now();
    d3.json(firstTreeResource, function (error, tree) {
        let end = performance.now();
        console.log('server took ', end - start, ' ms to respond to the first tree resource');

        if (error) {
            // TODO: probably best to handle the error with a user friendly message instead of throwing
            throw error;
        }
        trees[0] = tree

        start = performance.now();
        d3.json(secondTreeResource, function (error, tree) {
            let end = performance.now();
            console.log('server took ', end - start, ' ms to respond to the second tree resource');

            if (error) {
                // TODO: probably best to handle the error with a user friendly message instead of throwing
                throw error;
            }

            trees[1] = tree

            start = performance.now();
            d3.json(treeDifferencesResource, function (error, treeDifferences) {
                if (error) {
                    console.log('There has been a problem with your fetch operation: ' + error.message);
                    throw error;
                }

                currentTreeDifferences = treeDifferences

                end = performance.now();
                console.log('server took ', end - start, ' ms to respond to the tree differences');
                $('#loading-screen').modal('hide');

                $('#loading-screen').modal('hide');
                if (!isSlidingWindow)
                    buildPairComparison()
                else
                    buildSlidingComparison()

            })
        });
    });
}

function buildPairComparison() {
    isSlidingWindow = false
    $('#sliding-svg-article').hide()
    $('#pair-svgs-article').show()

    if (trees[1].constructor == Array)
        markDifferencesOnForest(trees[1], currentTreeDifferences)
    else
        markDifferencesOnTree(trees[1], currentTreeDifferences)

    defineColorPalette(numberOfDifferences, currentPaletteName)

    let svgHeight = window.outerHeight * 0.8;

    let svg1 = d3.select("#svg1")
        .attr("width", "100%")
        .attr("height", svgHeight)

    svg1.selectAll('*').remove();
    visualizations[0] = new Visualization();
    visualizations[0].initVisualization(trees[0], svg1, currentPalette);

    let svg2 = d3.select("#svg2")
        .attr("width", "100%")
        .attr("height", svgHeight)

    svg2.selectAll('*').remove();
    visualizations[1] = new Visualization();
    visualizations[1].initVisualization(trees[1], svg2, currentPalette);
    currentDifferencesSvg = svg2
    defineColorsLegend()

    slidingPairWindow.enable();
}

function buildSlidingComparison() {
    isSlidingWindow = true
    $('#pair-svgs-article').hide()
    $('#sliding-svg-article').show()

    if (trees[1].constructor == Array)
        markDifferencesOnForest(trees[1], currentTreeDifferences)
    else
        markDifferencesOnTree(trees[1], currentTreeDifferences)

    defineColorPalette(numberOfDifferences, currentPaletteName)

    let svgHeight = window.outerHeight * 0.8;

    let svg1 = d3.select("#sliding-svg")
        .attr("width", "100%")
        .attr("height", svgHeight)

    svg1.selectAll('*').remove();
    visualizations[0] = new Visualization();
    visualizations[0].initVisualization(trees[1], svg1, currentPalette);
    currentDifferencesSvg = svg1
    defineColorsLegend()

    slidingWindow.enable();
}

/**
 * Receives a forest and marks the differences present on treeDifferences
 * to color it according to the state in the visualization
 */
function markDifferencesOnForest(forest, treeDifferences) {
    forest.forEach((tree) => markDifferencesOnTree(tree, treeDifferences))
}

/**
 * Receives a root and marks the differences present on treeDifferences
 * to color it according to the state in the visualization
 */
function markDifferencesOnTree(tree, treeDifferences) {

    function recursiveMark(curNode, father) {

        //Node is new
        if (treeDifferences.addedVertices.some((vertex) => vertex.id == curNode.id)) {
            curNode.state = 2
            curNode.linkstate = 2
        }
        //Father node is new, therefore my link to him is also new
        else if (father && treeDifferences.addedVertices.some((vertex) => vertex.id == father.id)) {
            curNode.linkstate = 2
        }
        //Node changed place
        else if (treeDifferences.addedEdges.some((edge) => edge.vertexChild.id == curNode.id))
            curNode.linkstate = 1
        else if (father && treeDifferences.addedEdges.some((edge) => edge.vertexParent.id == curNode.id && edge.vertexChild.id == father.id))
            curNode.linkstate = 1

        if (curNode.children)
            curNode.children.forEach(child => recursiveMark(child, curNode))

        checkAndMarkRemovedEdges(treeDifferences, curNode)
    }

    recursiveMark(tree.root, undefined)

    if (tree.type == DENDROGRAM_TREE_TYPE)
        recursiveMarkTriangles(tree.root)
}

function recursiveMarkTriangles(node) {
    let collapsedState;
    let highestCollapsedState = 0;
    let prevCollapsedState = 0;

    //Retrieve the children highest state, 1 for moved edge and 2 for inserted, inserted has priority   
    if (node.children) {
        for (let i = 0; i < node.children.length; i++) {
            collapsedState = recursiveMarkTriangles(node.children[i])
            if (prevCollapsedState && prevCollapsedState < collapsedState)
                highestCollapsedState = collapsedState
            if (!prevCollapsedState && collapsedState)
                highestCollapsedState = collapsedState
            prevCollapsedState = collapsedState
        }
    }

    let nodeState = node.state ? node.state : 0
    let linkState = node.linkstate ? node.linkstate : 0

    //Define highest state as collapsible state
    if (highestCollapsedState > nodeState) {
        if (highestCollapsedState > linkState)
            node.collapsedstate = highestCollapsedState
        else
            node.collapsedstate = linkState
    }
    else {
        if (linkState > highestCollapsedState)
            node.collapsedstate = linkState
        else
            node.collapsedstate = highestCollapsedState
    }

    return node.collapsedstate
}

function checkAndMarkRemovedEdges(treeDifferences, curNode) {
    let removedEdges = treeDifferences.removedEdges.filter((edge) => edge.vertexParent.id == curNode.id)
    if (removedEdges.length >= 1) {
        let removedVertices = treeDifferences.removedVertices.filter((vertex) => removedEdges.some((edge) => edge.vertexChild.id == vertex.id))

        if (removedVertices.length < 1)
            return

        removedVertices.forEach((removedVertex) => {
            let removedEdge = removedEdges.filter((edge) => edge.vertexChild.id == removedVertex.id)

            if (removedEdge.length < 1)
                return

            if (curNode.children && curNode.children.filter((child) => child.id == removedEdge[0].vertexChild.id).legth > 0)
                return

            if (!curNode.children)
                curNode.children = []

            curNode.children.push({
                name: removedVertex.data,
                id: removedVertex.id,
                branch_length: removedEdge[0].length,
                distanceToRoot: curNode.distanceToRoot + removedEdge[0].length,
                removed: true
            })
        })

    }
}

function defineColorPalette(numberOfDifferences, paletteName) {
    //We will have 2 colors for each difference due to the collapsed and expanded node different colors
    let colorPalette = palette(paletteName, numberOfDifferences * 2 + 2)
    let baseColorPalette = ["181818", "d4d4d4"]

    //Skip the first 2 elements because the colors will be very light
    //TODO: better approach than skip the first 2 elements
    currentPalette = baseColorPalette.concat(colorPalette.slice(1, colorPalette.length - 1)).map((color) => "#" + color)
    return currentPalette
}

function defineColorsLegend() {
    legendData = [{
        "name": "New branch",
        "dash": "",
        "color": currentPalette[4],
    }, {
        "name": "Moved branch",
        "dash": "",
        "color": currentPalette[2]
    }, {
        "name": "Removed branch",
        "dash": ("9, 3"),
        "color": currentPalette[0]
    }];

    let legendLines = [{
        "line": 0
    }, {
        "line": 1
    }, {
        "line": 2
    }];

    let legend = currentDifferencesSvg.selectAll(".legend")
        .data(legendLines)
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function (d) {
            return "translate(" + 8 + "," + (d.line * 25 + 8) + ")";
        });

    legend.append("line")
        .attr("class", "legendrect")
        .attr("x1", 0)
        .attr("y1", 9)
        .attr("x2", 18)
        .attr("y2", 9)
        .style("stroke-width", 4)
        .style("stroke-dasharray", function (d) {
            return legendData[d.line].dash;
        })
        .style("stroke", function (d) {
            return legendData[d.line].color;
        });

    legend.append("text")
        .attr("x", 18)
        .attr("y", 9)
        .attr("dx", "2px")
        .attr("dy", ".35em")
        .style("text-anchor", "start")
        .style("font-family", "ubuntu, verdana, arial")
        .text(function (d) {
            return legendData[d.line].name;
        })
}

function updateLegendColors() {
    legendData = [{
        "name": "New branch",
        "dash": "",
        "color": currentPalette[4],
    }, {
        "name": "Moved branch",
        "dash": "",
        "color": currentPalette[2]
    }, {
        "name": "Removed branch",
        "dash": ("9, 3"),
        "color": currentPalette[0]
    }];

    currentDifferencesSvg.selectAll(".legendrect")
        .style("stroke", function (d) {
            return legendData[d.line].color;
        });
}

function onFirstTreeSearchNodeClick() {
    visualizations[0].searchNodeAndExpandThePathToIt(document.getElementById('search-node-input-1').value)
}

function onSecondTreeSearchNodeClick() {
    visualizations[1].searchNodeAndExpandThePathToIt(document.getElementById('search-node-input-2').value)
}

function onSlidingTreeSearchNodeClick() {
    visualizations[0].searchNodeAndExpandThePathToIt(document.getElementById('search-node-input-sliding').value)
}

function changeColorPalette(palette) {
    if (isSlidingWindow) {
        visualizations[0].changeColorPalette(palette)
        updateLegendColors()
    }
    else {
        visualizations[1].changeColorPalette(palette)
        updateLegendColors()
    }
}

function choosePalette(self) {
    let paletteName = palettes[self.checked ? 1 : 0]
    currentPaletteName = paletteName
    defineColorPalette(numberOfDifferences, paletteName)
    changeColorPalette(currentPalette)
}

function changeBranchLengthRatio(visualization, increment) {
    visualizations[visualization].changeBranchLengthRatio(increment)
}

function changeBranchHeightRatio(visualization, increment) {
    visualizations[visualization].changeBranchHeightRatio(increment)
}

function buildSlidingPairWindow(tsA, tsB) {
    if (typeof Slider === "undefined")
        return

    slidingPairWindow = new Slider('#sliding-pair-window', {
        tooltip_position: "bottom"
    })

    slidingPairWindow.oldValue = [tsA, tsB]
    slidingPairWindow.hasChanged = function (now) {
        const old = this.oldValue
        this.oldValue = now;

        return old[0] !== now[0] || old[1] !== now[1];
    }

    slidingPairWindow.on('slideStop', onSlidePairStop)
}

function onSlidePairStop(mEvt) {
    if (!slidingPairWindow.hasChanged(mEvt)) {
        return false;
    }

    slidingPairWindow.disable()

    const ts = slidingPairWindow.getValue()
    loadTrees(
        buildTreeUri(ts[0]),
        buildTreeUri(ts[1]),
        buildTreesDiffUri(ts[0], ts[1])
    );

    return false;
}

function buildSlidingWindow(ts) {
    if (typeof Slider === "undefined")
        return

    slidingWindow = new Slider('#sliding-window', {
        tooltip_position: "bottom"
    })

    slidingWindow.hasChanged = function (now) {
        const old = this.oldValue
        this.oldValue = now;

        return old !== now;
    }

    slidingWindow.on('slideStop', onSlideStop)
}

function onSlideStop(mEvt) {
    if (!slidingWindow.hasChanged(mEvt)) {
        return false;
    }

    slidingWindow.disable()

    const tb = slidingWindow.getValue()
    const ta = tb <= 0 ? 0 : tb - 1

    loadTrees(
        buildTreeUri(ta),
        buildTreeUri(tb),
        buildTreesDiffUri(ta, tb)
    );

    return false;
}

function onPageLoad(apiUri, _treeID, tsA, tsB) {
    apiHost = apiUri;
    treeID = _treeID;
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
    buildSlidingPairWindow(tsA, tsB)
    buildSlidingWindow(tsB)
    loadTrees(
        buildTreeUri(tsA),
        buildTreeUri(tsB),
        buildTreesDiffUri(tsA, tsB)
    );
}

function buildTreeUri(ts) {
    return apiHost + '/tree/' + treeID + '/timestamp/' + ts
}

function buildTreesDiffUri(tsA, tsB) {
    return apiHost + '/tree/' + treeID + '/diff?ta=' + tsA + '&tb=' + tsB
}

function _exportNewick(tree) {
    if (isSlidingWindow)
        exportNewick(buildTreeUri(slidingWindow.getValue()))
    else
        exportNewick(buildTreeUri(slidingPairWindow.getValue()[tree]))
}

function changeViewMode() {
    isSlidingWindow = !isSlidingWindow

    if (isSlidingWindow) {
        const ts = slidingPairWindow.getValue()
        slidingWindow.setValue(ts[1])
        loadTrees(
            buildTreeUri(ts[0]),
            buildTreeUri(ts[1]),
            buildTreesDiffUri(ts[0], ts[1])
        );
    }
    else {
        const tb = slidingWindow.getValue()
        const ta = tb <= 0 ? 0 : tb - 1
        slidingPairWindow.setValue([ta, tb])
        loadTrees(
            buildTreeUri(ta),
            buildTreeUri(tb),
            buildTreesDiffUri(ta, tb)
        );
    }
}