$(function () {
    $('#view-dif-form').submit(function () {
        var tid = $('#diftreeId').val();
        var ts1 = $('#firstTs').val();
        var ts2 = $('#secondTs').val();
        $(this).attr('action', `/view/${tid}/timestamp/${ts1}/${ts2}`);
    });
});

$(function () {
    $('#view-form').submit(function () {
        var tid = $('#treeId').val();
        $(this).attr('action', `/view/${tid}/timestamp/max`);
    });
});