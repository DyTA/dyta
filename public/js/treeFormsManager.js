const ADD_PARENTVERTEXID = "apvid"
const ADD_NEWVERTEXID = "anvdata"
const ADD_NEWBRANCHLENGTHID = "ablength"
const REMOVE_VERTEXIDTOREMOVE = "rvid"
const UPDATE_VERTEXID = "uvid"
const UPDATE_VERTEXDATA = "uvdata"
const CHANGESHISTORY_TEXTAREA_ID = "changes-history"
const MANUALCHANGES_TEXTAREA_ID = "manual-changes-text"
const NEWICKCHANGES_TEXT_ID = "newick-text"

const ADD_ACTIONS_FIELDNAME = "add"
const UPDATE_ACTIONS_FIELDNAME = "update"
const REMOVE_ACTIONS_FIELDNAME = "remove"

let changes = {};
let generatedID = -1;

function addBranch() {
    let form = document.forms["addBranchForm"]

    let parentVertexId = form[ADD_PARENTVERTEXID].value
    let newVertexData = form[ADD_NEWVERTEXID].value
    let newBranchLength = form[ADD_NEWBRANCHLENGTHID].value

    if (!changes[ADD_ACTIONS_FIELDNAME])
        changes[ADD_ACTIONS_FIELDNAME] = []

    changes[ADD_ACTIONS_FIELDNAME].push({
        generatedID: generatedID,
        parentVid: parseInt(parentVertexId),
        vertexData: newVertexData,
        branchLength: newBranchLength
    })

    generatedID -= 1;

    document.getElementById(CHANGESHISTORY_TEXTAREA_ID).value = JSON.stringify(changes)
    form.reset()
}

function removeBranch() {
    let form = document.forms["removeBranchForm"]
    let vertexIdToRemove = form[REMOVE_VERTEXIDTOREMOVE].value

    if (!changes[REMOVE_ACTIONS_FIELDNAME])
        changes[REMOVE_ACTIONS_FIELDNAME] = []

    changes[REMOVE_ACTIONS_FIELDNAME].push({ vid: vertexIdToRemove })
    document.getElementById(CHANGESHISTORY_TEXTAREA_ID).value = JSON.stringify(changes)
    form.reset()
}

function updateBranch() {
    let form = document.forms["updateBranchForm"]

    let vertexId = form[UPDATE_VERTEXID].value
    let vertexData = form[UPDATE_VERTEXDATA].value

    if (!changes[UPDATE_ACTIONS_FIELDNAME])
        changes[UPDATE_ACTIONS_FIELDNAME] = []

    changes[UPDATE_ACTIONS_FIELDNAME].push({
        vid: vertexId,
        vertexData
    })
    document.getElementById(CHANGESHISTORY_TEXTAREA_ID).value = JSON.stringify(changes)
    form.reset()
}

function manualChanges() {
    let form = document.forms["manualChangesForm"]

    let jsonChanges = form[MANUALCHANGES_TEXTAREA_ID].value

    try {
        changes = JSON.parse(jsonChanges)
    }
    catch (err) {
        alertify.error("Invalid format")
    }

    document.getElementById(CHANGESHISTORY_TEXTAREA_ID).value = JSON.stringify(changes)
    form.reset();
}

function isValidAddAction(add) {
    if (!add.branchLength) {
        alertify.error("Please provide a length for the branch on add")
        return false
    }

    if (add.branchLength < 0) {
        alertify.error("Please provide a positive length for the branch on add, branch length \"" + add.branchLength + "\" is not valid")
        return false
    }

    return true
}

function isValidUpdateAction(update) {
    if (update.vid < 0) {
        alertify.error("Please provide a valid vertex id on update, id \"" + update.vid + "\" is not valid")
        return false
    }
    return true
}

function isValidRemoveAction(remove) {
    if (remove.vid < 0) {
        alertify.error("Please provide a valid vertex id on remove, id \"" + remove.vid + "\" is not valid")
        return false
    }
    return true
}

function areValidChanges(changes){
    let areValid = true
    if (!changes.add && !changes.update && !changes.remove)
        areValid = false
    if (changes.add && !changes.add.every(isValidAddAction))
        areValid = false
    if (changes.update && !changes.update.every(isValidUpdateAction))
        areValid = false
    if (changes.remove && !changes.remove.every(isValidRemoveAction))
        areValid = false
    
    return areValid
}

function sendManualChanges(uri){
    if(!areValidChanges(changes)){
        alertify.error('Your changes aren\'t valid :(')
        return
    }

    changes.type = "manual"
    generatedID = -1;
    sendChanges(uri, changes)
}

function clearChanges(){
    document.getElementById(CHANGESHISTORY_TEXTAREA_ID).value = ""
    changes = {}
}

function sendNewickChanges(uri){
    changes.type = "newick"
    changes.newickTree = document.getElementById(NEWICKCHANGES_TEXT_ID).value
    sendChanges(uri, changes)
}

function sendChanges(uri, changes) {
    jsonPost(uri, changes)
    .then(data => {
        console.log(data)
        window.location = `/view/${data.tid}/timestamp/${data.ts}`
    })
    .catch(error => alertify.error('An error occured when we tried to submit your changes, please try again'))
}

function jsonPost(uri, jsonObj) {
    const req = {
        method: 'POST',
        headers: { 'accept': 'application/json', 'content-Type': 'application/json' },
        mode: 'same-origin',
        body: JSON.stringify(jsonObj),
    }

    return fetch(uri, req)
        .then(response => {
            if (response.ok) {
                return response.json();
            }

            throw new Error('Failure to get response from server')
        })
}

function toggleUpdateForm(){
    function toggleHidden(id) {
        const element = document.getElementById(id);
        const hidden = element.hasAttribute('hidden')
        
        hidden ? element.removeAttribute('hidden') : element.setAttribute('hidden', '')
    }

    toggleHidden('manual-update-section')
    toggleHidden('newick-update-section')
    toggleHidden("nwk-btn-text")
    toggleHidden("man-btn-text")
}