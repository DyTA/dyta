const CENTER_TRANSITION_DURATION = 850
const DRAW_TRANSITION_DURATION = 350

const PATH_STROKE_WIDTH = 2
const BRANCH_HEIGHT = 20
const NODE_RADIUS = 4
const LEAF_RADIUS = 5
const TEXT_DISTANCE_FROM_LEAF = 13
const NODE_TEXT_SIZE = 12
const NODE_TEXT_FONT = "ubuntu, verdana, arial"
const NUMBER_OF_LEVELS = 2 //after trial and error seemed the best number

// The class that saves the tree state and has the functions directly related to the tree state
class Visualization {
    constructor() {
        this.i = 0
        this.branchLengthRatio
        this.treeWidth
        this.treeHeight
        this.colorPalette = ["#181818", "#d4d4d4"]
        this.svgGroup
        this.tree
        this.treeMaxDepth
        this.root
        this.svgWidth
        this.svgHeight
        this.inputBranchLengthRatio = 1
        this.inputBranchHeightRatio = 1
        let thisContext = this
        this.zoomListener = d3.behavior.zoom()
            .on("zoom", function () {
                return thisContext.defaultZoom.call(thisContext);
            })
    }

    initVisualization(treeFromJson, svg, colorPalette) {
        this.svgWidth = parseInt(svg.style("width"))
        this.svgHeight = parseInt(svg.style("height"))

        if (colorPalette)
            this.colorPalette = colorPalette

        // attach a class for styling and the zoomListener
        svg.attr("class", "overlay")
            .call(this.zoomListener)
            .on("dblclick.zoom", null);

        // Append a group which holds all nodes and which the zoom Listener can act upon.
        this.svgGroup = svg.append("g");

        /**
         * in nodeSize the first index defines the height of each node, 
         * the second defines the width, since width will be changed according to svgWidth
         * it is irrelevant what we pass
         */
        this.tree = d3.layout.cluster()
            .nodeSize([BRANCH_HEIGHT * this.inputBranchHeightRatio, 0])
        this.treeWidth = this.svgWidth - this.svgWidth / 10

        this.treeMaxDepth = treeFromJson[TREE_MAX_LENGTH_FIELD_NAME]

        this.root = treeFromJson.root;

        this.root.children.forEach(collapse) //<- in case you don't want to show the children from the start

        this.update(this.root);
        centerTreeOnSvg(this.root, this.treeWidth, this.svgWidth, this.svgHeight, this.zoomListener, this.svgGroup)
    }

    update(source) {
        let nodesPerLevel = this.countNodesPerLevel(this.root)

        // Compute the new tree layout.
        let nodes = this.tree.nodes(this.root).reverse(),
            links = this.tree.links(nodes);

        this.branchLengthRatio = (this.treeWidth * this.inputBranchLengthRatio) / this.treeMaxDepth;
        let branchLengthRatio = this.branchLengthRatio
        // Set widths between levels based on maxLabelLength.
        nodes.forEach(function (d) {
            d.y = d[NODE_DEPTH_FIELD_NAME] * branchLengthRatio
        });

        this.updateNodes(source, nodes);
        this.updateLinks(source, links);

        // Stash the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    updateNodes(source, nodes) {
        let thisContext = this
        // Update the nodes…
        let node = this.svgGroup.selectAll("g.node")
            .data(nodes, function (d) {
                return d.id;
            });

        // Enter any new nodes at the parent's previous position.
        let nodeEnter = node.enter().append("g")
            .attr("class", "node")
            .attr("transform", function (d) {
                return "translate(" + source.y0 + "," + source.x0 + ")";
            })
            .on('click',
            function (d) {
                return thisContext.click.call(thisContext, d);
            });

        // Add Triangles to collapsible nodes
        nodeEnter.append('polyline')
            .attr('class', 'node')
            .attr('points', function (d) {
                return thisContext.calculateTrianglePoints.call(thisContext, d);
            })
            .style("fill", function (d) {
                let color = thisContext.setNodeFillColor.call(thisContext, d);
                return color
            })
            .style("stroke", function (d) {
                return thisContext.setNodeStrokeColor.call(thisContext, d);
            })
            .style("stroke-width", PATH_STROKE_WIDTH);

        // Add Circle for the nodes
        nodeEnter.append('circle')
            .attr('class', 'node')
            .attr('r', this.setNodeRadius)
            .style("fill", function (d) {
                return thisContext.setNodeFillColor.call(thisContext, d);
            })
            .style("stroke", function (d) {
                return thisContext.setNodeStrokeColor.call(thisContext, d);
            });

        // Show vertex id when hovering
        nodeEnter.append("svg:title")
            .text(function (d) {
                return "Vertex id: " + d[NODE_ID_NAME_FIELD];
            });

        // Add labels for the nodes
        nodeEnter.append('text')
            .attr("dy", ".35em")
            .attr("x", function (d) {
                return thisContext.setNodeTextDistance.call(thisContext, d);
            })
            .attr("text-anchor", function (d) {
                return "start";
            })
            .attr("font", function (d) {
                return NODE_TEXT_SIZE + "px " + NODE_TEXT_FONT;
            })
            .text(function (d) { return d[NODE_NAME_FIELD]; });

        // Transition nodes to their new position.
        let nodeUpdate = node.transition()
            .duration(DRAW_TRANSITION_DURATION)
            .attr("transform", function (d) {
                return "translate(" + d.y + "," + d.x + ")";
            });

        /* 
        * Without these three updates if you fast 
        * double click in a node
        * the node and the text disappear
        */
        nodeUpdate.select('circle.node')
            .attr('r', this.setNodeRadius)
            .style("fill", function (d) {
                return thisContext.setNodeFillColor.call(thisContext, d);
            })
            .style("stroke", function (d) {
                return thisContext.setNodeStrokeColor.call(thisContext, d);
            })
            .attr('cursor', 'pointer');

        nodeUpdate.select('polyline.node')
            .attr('points', function (d) {
                return thisContext.calculateTrianglePoints.call(thisContext, d);
            })
            .style("fill", function (d) {
                return thisContext.setNodeFillColor.call(thisContext, d);
            })
            .style("stroke", function (d) {
                return thisContext.setNodeStrokeColor.call(thisContext, d);
            })
            .attr('cursor', 'pointer');

        nodeUpdate.select("text")
            .text(this.setNodeText)
            .attr("x", function (d) {
                return thisContext.setNodeTextDistance.call(thisContext, d);
            })
            .style("fill-opacity", 1);

        // Remove any exiting nodes
        let nodeExit = node.exit().transition()
            .duration(DRAW_TRANSITION_DURATION)
            .attr("transform", function (d) {
                return "translate(" + source.y + "," + source.x + ")";
            })
            .remove();

        // On exit reduce the node circles size to 0
        nodeExit.select('circle')
            .attr('r', 1e-6);

        // On exit reduce the opacity of text labels
        nodeExit.select('text')
            .style('fill-opacity', 1e-6);
    }

    updateLinks(source, links) {
        let thisContext = this
        // Update the links…
        let link = this.svgGroup.selectAll("path.link")
            .data(links, function (d) {
                return d.target.id;
            });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .style("stroke", function (d) {
                return thisContext.setLinkStrokeColor.call(thisContext, d);
            })
            .style("stroke-width", PATH_STROKE_WIDTH)
            .style("stroke-dasharray", thisContext.setDash)
            .attr('d', elbow)
            .append("svg:title")
            .text(function (link) {
                return "Branch length: " + link.target[BRANCH_LENGTH_NAME_FIELD]
            });

        // Transition links to their new position.
        link.transition()
            .duration(DRAW_TRANSITION_DURATION)
            .attr('d', elbow)

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(DRAW_TRANSITION_DURATION)
            .style("stroke", function (d) {
                return thisContext.setLinkStrokeColor.call(thisContext, d);
            })
            .attr('d', exitElbow)
            .remove();

            /*
        // Update the link text
        var linktext = this.svgGroup.selectAll("g.link")
            .data(links, function (d) {
                return d.target.id;
            });

        linktext.enter()
            .insert("g")
            .attr("class", "link")
            .append("text")
            .attr("x", function (link) {
                return link.target.y;
            })
            .attr("y", function (link) {
                return link.target.x;
            })
            .attr("fill", "Black")
            .attr("font", function (d) {
                return NODE_TEXT_SIZE + "px " + NODE_TEXT_FONT;
            })
            .text(function (link) {
                return link.target[BRANCH_LENGTH_NAME_FIELD];
            });

        // Transition link text to their new positions

        linktext.transition()
            .duration(DRAW_TRANSITION_DURATION)
            .attr("transform", function (d) {
                return "translate(" + 0 + "," + 0 + ")";
            })

        //Transition exiting link text to the parent's new position.
        linktext.exit().transition()
            .remove();*/

    }

    changeColorPalette(colorPalette) {
        let thisContext = this
        this.colorPalette = colorPalette

        //Recolor circles
        this.svgGroup.selectAll("circle")
            .style("fill", function (d) {
                let color = thisContext.setNodeFillColor.call(thisContext, d);
                return color
            })
            .style("stroke", function (d) {
                return thisContext.setNodeStrokeColor.call(thisContext, d);
            })

        //Recolor triangles
        this.svgGroup.selectAll("polyline")
            .style("fill", function (d) {
                let color = thisContext.setNodeFillColor.call(thisContext, d);
                return color
            })
            .style("stroke", function (d) {
                return thisContext.setNodeStrokeColor.call(thisContext, d);
            })

        //Recolor links
        this.svgGroup.selectAll(".link").
            style("stroke", function (d) {
                return thisContext.setLinkStrokeColor.call(thisContext, d);
            })
    }

    //Function called on click event
    click(d) {
        if (d3.event.ctrlKey)
            d = collapseOrExpand(d);
        else
            d = collapseOrSingleExpand(d)
        this.update(d);
        centerSvgOnNode(d, this.svgWidth, this.svgHeight, this.zoomListener, this.svgGroup);
    }

    //Function called on zoom event
    defaultZoom() {
        this.svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }

    countNodesPerLevel(root) {
        let nodesPerLevel = new Array(NUMBER_OF_LEVELS).fill(0);
        this.countChildNodesPerLevel(root, nodesPerLevel);
        return nodesPerLevel;
    }

    countChildNodesPerLevel(node, nodesPerLevel) {
        if (node.children)
            node.children.forEach((child) => {
                this.countChildNodesPerLevel(child, nodesPerLevel);
            })

        let depth = node[NODE_DEPTH_FIELD_NAME];
        let i = 0;
        for (; depth > this.treeMaxDepth / NUMBER_OF_LEVELS; i++)
            depth -= this.treeMaxDepth / NUMBER_OF_LEVELS;
        nodesPerLevel[i] += 1
    }

    searchNodeAndExpandThePathToIt(nodeName) {
        let nodes = this.searchNodesToExpandFrom(this.root, nodeName)
        if (nodes) {
            //idx = 1 in order not to expand the searched node, that is always at idx = 0
            for (let idx = 1; idx < nodes.length; idx++)
                singleLevelExpand(nodes[idx])

            this.update(this.root)
            centerSvgOnNode(nodes[0], this.svgWidth, this.svgHeight, this.zoomListener, this.svgGroup);
        } else {
            alertify.error("The desired node couldn't be found in this tree :(")
        }
    }

    /**
     * Function that searches for a node with a given name and 
     * returns an array with the nodes that links the root to 
     * the node or null if it didn't find the desired node
     * The array has the searched node in first index and
     * the other nodes on the rest of the indexes
     * @param currentNode the current node we are searching on
     * @param nodeName the name of the node we are searching for
     * @return the array with the pathway nodes
     */
    searchNodesToExpandFrom(currentNode, nodeName) {
        let nodesArray = []
        if (this.internalSearchNodesToExpandFrom(currentNode, nodeName, nodesArray))
            return nodesArray;
        return null;
    }

    /**
     * Internal function that searches for a node with a given name
     * saving the nodes it passed through on nodesArray parameter
     * @param currentNode the current node we are searching on
     * @param nodeName the name of the node we are searching for
     * @param nodesArray the array with the pathway nodes
     * @return true or false whether it found the node or not
     */
    internalSearchNodesToExpandFrom(currentNode, nodeName, nodesArray) {
        if (currentNode[NODE_NAME_FIELD] == nodeName) {
            nodesArray.push(currentNode)
            return true;
        }
        if (currentNode._children) {
            for (let i = 0; i < currentNode._children.length; i++) {
                if (this.internalSearchNodesToExpandFrom(currentNode._children[i], nodeName, nodesArray)) {
                    nodesArray.push(currentNode)
                    return true
                }
            }
        }
        if (currentNode.children) {
            for (let i = 0; i < currentNode.children.length; i++)
                if (this.internalSearchNodesToExpandFrom(currentNode.children[i], nodeName, nodesArray)) {
                    nodesArray.push(currentNode)
                    return true
                }
        }
        return false;
    }

    changeBranchLengthRatio(change) {
        if ((this.inputBranchLengthRatio + change) <= 0)
            return

        this.inputBranchLengthRatio += change
        this.update(this.root)
    }

    changeBranchHeightRatio(change) {
        if ((this.inputBranchHeightRatio + change) <= 0)
            return

        this.inputBranchHeightRatio += change
        this.tree = this.tree.nodeSize([BRANCH_HEIGHT * this.inputBranchHeightRatio, 0])
        this.update(this.root)
    }

    calculateTrianglePoints(node) {
        if (isCollapsible(node)) {
            let distanceFromLeaf = this.treeMaxDepth * this.branchLengthRatio - node.y
            let halfTriangleHeight = LEAF_RADIUS

            let x0 = -1;
            let y0 = 0;
            let x1 = (this.treeMaxDepth - node[NODE_DEPTH_FIELD_NAME]) * this.branchLengthRatio + LEAF_RADIUS
            let y1 = halfTriangleHeight
            let x2 = x1
            let y2 = -halfTriangleHeight

            return x0 + ' ' + y0 + ', ' + x1 + ' ' + y1 + ', ' + x2 + ' ' + y2 + ', ' + x0 + ' ' + y0;
        }
        return "";
    }

    calculateTrianglePoints2(node) {
        if (isCollapsible(node)) {
            let distanceFromLeaf = this.treeMaxDepth * this.branchLengthRatio - node.y
            let halfTriangleHeight = Math.log2(node.nChildren) + BRANCH_HEIGHT / 2

            let x0 = 0;
            let y0 = 0;
            let x1 = (this.treeMaxDepth - node[NODE_DEPTH_FIELD_NAME]) * this.branchLengthRatio + LEAF_RADIUS
            let y1 = halfTriangleHeight
            let x2 = x1
            let y2 = -halfTriangleHeight

            return x0 + ' ' + y0 + ', ' + x1 + ' ' + y1 + ', ' + x2 + ' ' + y2 + ', ' + x0 + ' ' + y0;
        }
        return "";
    }

    setNodeFillColor(d) {
        if (!isNaN(d.state))
            return isCollapsible(d) ? this.colorPalette[d.state * 2 + 1] : this.colorPalette[d.state * 2];
        else
            return isCollapsible(d) ? this.colorPalette[1] : this.colorPalette[0];
    }

    setNodeFillColor(d) {
        if (!isNaN(d.state))
            return isCollapsible(d) ? this.colorPalette[d.state * 2 + 1] : this.colorPalette[d.state * 2];
        else {
            if(isCollapsible(d))
                return d.collapsedstate ? this.colorPalette[d.collapsedstate * 2] : this.colorPalette[1]
            else
                return this.colorPalette[0]
        }
    }

    setNodeStrokeColor(d) {
        return !isNaN(d.state) ? this.colorPalette[d.state * 2] : this.colorPalette[0];
    }

    setLinkStrokeColor(d) {
        return !isNaN(d.target.linkstate) ? this.colorPalette[d.target.linkstate * 2] : this.colorPalette[0];
    }

    setNodeTextDistance(node) {
        if (!isLeaf(node)) {
            let distanceFromLeaf = this.treeMaxDepth * this.branchLengthRatio - node.y
            return distanceFromLeaf + TEXT_DISTANCE_FROM_LEAF;
        }
        else
            return TEXT_DISTANCE_FROM_LEAF;
    }

    setNodeText(d) {
        if (d._children && d._children.length > 0)
            return d[NODE_NUMBER_OF_CHILDREN_FIELD_NAME]
        else if (!d.children || (d.children.length == 0))
            return d[NODE_NAME_FIELD];
        return "";
    }

    setNodeRadius(d) {
        if (!isLeaf(d))
            return NODE_RADIUS;
        else
            return LEAF_RADIUS;
    }

    setDash(link) {
        return link.target.removed ? ("3, 3") : ""
    }
}

/**
 * Auxiliary functions that are not directly related to the tree visualization state
 */

// Function to center visualization on node
function centerSvgOnNode(source, svgWidth, svgHeight, zoomListener, svg) {
    let scale = zoomListener.scale();
    let x = source.y0 * scale;
    let y = source.x0 * scale;
    let translateX = svgWidth / 2 - x;
    let translateY = svgHeight / 2 - y;
    svg.transition()
        .duration(CENTER_TRANSITION_DURATION)
        .attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
    zoomListener.scale(scale);
    zoomListener.translate([translateX, translateY]);
}

// Function to center tree on svg
function centerTreeOnSvg(root, treeWidth, svgWidth, svgHeight, zoomListener, svg) {
    let scale = zoomListener.scale();
    let y = root.x0 * scale;
    let translateX = svgWidth / 2 - treeWidth / 2 * scale
    let translateY = svgHeight / 2 - y
    svg.transition()
        .duration(CENTER_TRANSITION_DURATION)
        .attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
    zoomListener.scale(scale);
    zoomListener.translate([translateX, translateY]);
}

function collapseOrExpand(d) {
    if (d.children) {
        d._children = d.children
        d._children.forEach(collapse)
        d.children = null
    } else if (d._children) {
        d.children = d._children
        d.children.forEach(expand)
        d._children = null
    }
    return d;
}

function collapseOrSingleExpand(d) {
    if (d.children) {
        d._children = d.children
        d._children.forEach(collapse)
        d.children = null
    } else if (d._children) {
        d.children = d._children
        d._children = null
    }
    return d;
}

function collapse(d) {
    if (d.children) {
        d._children = d.children;
        d._children.forEach(collapse);
        d.children = null;
    }
}

function expand(d) {
    if (d._children) {
        d.children = d._children;
        d.children.forEach(expand);
        d._children = null;
    }
}

function singleLevelExpand(d) {
    if (d._children) {
        d.children = d._children;
        d._children = null;
    }
}

function isCollapsible(node) {
    return node._children && (node._children.length > 0)
}

function isLeaf(node) {
    return !((node.children && node.children.length > 0) || (node._children && node._children.length > 0))
}

// Creates a elbow like path from parent to child node
function elbow(d) {
    return "M" + d.source.y + "," + d.source.x //tanto "," como " " sao aceites para separar a coordenada x do y
        + "V" + d.target.x
        + "H" + d.target.y;
}

// Used in exit transition of links
function exitElbow(d) {
    return "M" + d.source.y + "," + d.source.x
        + "V" + d.source.x
        + "H" + d.source.y;
}