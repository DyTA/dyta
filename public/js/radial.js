const CENTER_TRANSITION_DURATION = 850
const DRAW_TRANSITION_DURATION = 350

const PATH_STROKE_WIDTH = 2
const BRANCH_HEIGHT = 20
const NODE_RADIUS = 4
const LEAF_RADIUS = 5
const TEXT_DISTANCE_FROM_LEAF = 13
const NODE_TEXT_SIZE = 12
const NODE_TEXT_FONT = "ubuntu, verdana, arial"

class Visualization {
    constructor() {
        this.i = 0
        this.branchLengthRatio
        this.treeWidth
        this.treeHeight
        this.colorPalette = ["#181818", "#d4d4d4"]
        this.g
        this.tree
        this.treeMaxDepth
        this.root
        this.svgWidth
        this.svgHeight
        this.inputBranchLengthRatio = 1
        this.inputBranchHeightRatio = 1
        
		let thisContext = this
        this.zoomListener = d3.behavior.zoom()
            .on("zoom", function () {
                return thisContext.defaultZoom.call(thisContext);
            })
    }

    initVisualization(treeFromJson, svg, colorPalette) {
		// treeFromJson = Object { name: "new tree", type: 1, description: null, root: Object, longestDistance: 2.5555556320000004, numberOfLeaves: 10 }
        this.svgWidth = parseInt(svg.style("width"))
        this.svgHeight = parseInt(svg.style("height"))
		
        this.treeHeight = this.svgHeight - this.svgHeight / 10
        this.treeWidth = this.treeHeight

		if (colorPalette)
            this.colorPalette = colorPalette
		
		svg.attr("class", "overlay")
            .call(this.zoomListener)
            .on("dblclick.zoom", null);

		this.tree = d3.layout.tree()
			.size([360, this.treeHeight])
			.separation((a, b) => (a.parent == b.parent ? 1 : 10) / a.depth)

		this.diagonal = d3.svg.diagonal.radial().projection(function (d) {
			return [ d.y, d.x / 180 * Math.PI]
		})

        
		const radius = this.treeHeight / 2
		this.g = svg.append("g")
			.attr('transform', 'translate('+radius+','+radius+')');

		this.root = treeFromJson.root
		this.root.x0 =  this.svgHeight  / 2
		this.root.y0 = 0

		this.update(this.root)
    }

    update(source) {
        let nodes = this.tree.nodes(source)
		let links = this.tree.links(nodes)

		const self = this

		this.updateNodes(source, nodes)
		this.updateLinks(source, links)

		nodes.forEach(function(d) {
			d.x0 = d.x;
			d.y0 = d.y;
		});
    }

	updateNodes(source, nodes) {
		const self = this
		let node = this.g.selectAll("g.node").data(nodes, d => d.id);

		let nodeEnter = node
			.enter()
			.append("g")
            .attr("class", "node")
            .on('click', function (d) {
                return self.click.call(self, d);
            })
		
		nodeEnter
			.append('circle')
			.attr('r', this.setNodeRadius)

		nodeEnter
			.append('text')
			.attr('x', d => self.setNodeTextDistance.call(self, d))
			.style('fill-opacity', 1e-6)

		const nodeUpdate = node
			.transition()
			.duration(DRAW_TRANSITION_DURATION)
			.attr("transform", d => "rotate(" + (d.x - 90) + ")translate(" + d.y + ")")
		
		// light blue color for nodes collapsed and with children
		nodeUpdate
			.select("circle")
			.attr("r", 4.5)
			.style("fill", "lightsteelblue")

		nodeUpdate
			.select("text")
			.style("fill-opacity", 1)
			.attr("transform", function(d) {
				return d.x < 180 ? "translate(0)" : "rotate(180)";
			});

		const nodeExit = node
			.exit()
			.transition()
			.duration(DRAW_TRANSITION_DURATION)
			.remove();

		nodeExit.select("circle").attr("r", 1e-6);

		nodeExit.select("text").style("fill-opacity", 1e-6);
	}

	updateLinks(source, links) {
		const self = this
		const link = this.g.selectAll("path.link").data(links, d => d.target.id )
		const diagonal = self.diagonal

		link
			.enter()
			.insert("path", "g")
			.attr("class", "link")
			.style('stroke', d => self.setLinkStrokeColor.call(self, d))
			.style("stroke-width", PATH_STROKE_WIDTH)
			.attr("d", function(d) {
				var o = { x: source.x0, y: source.y0 };
				return diagonal({ source: o, target: o });
			});

		link
			.transition()
			.duration(DRAW_TRANSITION_DURATION)
			.attr("d", diagonal);

		link
			.exit()
			.transition()
			.duration(DRAW_TRANSITION_DURATION)
			.style("stroke", d => self.setLinkStrokeColor.call(self, d))
			.attr("d", function(d) {
				var o = { x: source.x, y: source.y };
				return diagonal({ source: o, target: o });
			})
			.remove();
	}

    changeColorPalette(colorPalette) {
        let thisContext = this
        this.colorPalette = colorPalette

        //Recolor circles
        this.g.selectAll("circle")
            .style("fill", function (d) {
                let color = thisContext.setNodeFillColor.call(thisContext, d);
                return color
            })
            .style("stroke", function (d) {
                return thisContext.setNodeStrokeColor.call(thisContext, d);
            })

        //Recolor links
        this.g.selectAll(".link").
            style("stroke", function (d) {
                return thisContext.setLinkStrokeColor.call(thisContext, d);
            })
    }

    click(d) {
        if (d3.event.ctrlKey)
            d = collapseOrExpand(d);
        else
            d = collapseOrSingleExpand(d)

        this.update(d);
    }

    //Function called on zoom event
    defaultZoom() {
        this.g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }

    countNodesPerLevel(root) {
        let nodesPerLevel = new Array(NUMBER_OF_LEVELS).fill(0);
        this.countChildNodesPerLevel(root, nodesPerLevel);
        return nodesPerLevel;
    }

    countChildNodesPerLevel(node, nodesPerLevel) {
        if (node.children)
            node.children.forEach((child) => {
                this.countChildNodesPerLevel(child, nodesPerLevel);
            })

        let depth = node[NODE_DEPTH_FIELD_NAME];
        let i = 0;
        for (; depth > this.treeMaxDepth / NUMBER_OF_LEVELS; i++)
            depth -= this.treeMaxDepth / NUMBER_OF_LEVELS;
        nodesPerLevel[i] += 1
    }

    searchNodeAndExpandThePathToIt(nodeName) {
        let nodes = this.searchNodesToExpandFrom(this.root, nodeName)
        if (nodes) {
            //idx = 1 in order not to expand the searched node, that is always at idx = 0
            for (let idx = 1; idx < nodes.length; idx++)
                singleLevelExpand(nodes[idx])

            this.update(this.root)
            centerSvgOnNode(nodes[0], this.svgWidth, this.svgHeight, this.zoomListener, this.g);
        } else {
            alertify.error("The desired node couldn't be found in this tree :(")
        }
    }

    /**
     * Function that searches for a node with a given name and 
     * returns an array with the nodes that links the root to 
     * the node or null if it didn't find the desired node
     * The array has the searched node in first index and
     * the other nodes on the rest of the indexes
     * @param currentNode the current node we are searching on
     * @param nodeName the name of the node we are searching for
     * @return the array with the pathway nodes
     */
    searchNodesToExpandFrom(currentNode, nodeName) {
        let nodesArray = []
        if (this.internalSearchNodesToExpandFrom(currentNode, nodeName, nodesArray))
            return nodesArray;
        return null;
    }

    /**
     * Internal function that searches for a node with a given name
     * saving the nodes it passed through on nodesArray parameter
     * @param currentNode the current node we are searching on
     * @param nodeName the name of the node we are searching for
     * @param nodesArray the array with the pathway nodes
     * @return true or false whether it found the node or not
     */
    internalSearchNodesToExpandFrom(currentNode, nodeName, nodesArray) {
        if (currentNode[NODE_NAME_FIELD] == nodeName) {
            nodesArray.push(currentNode)
            return true;
        }
        if (currentNode._children) {
            for (let i = 0; i < currentNode._children.length; i++) {
                if (this.internalSearchNodesToExpandFrom(currentNode._children[i], nodeName, nodesArray)) {
                    nodesArray.push(currentNode)
                    return true
                }
            }
        }
        if (currentNode.children) {
            for (let i = 0; i < currentNode.children.length; i++)
                if (this.internalSearchNodesToExpandFrom(currentNode.children[i], nodeName, nodesArray)) {
                    nodesArray.push(currentNode)
                    return true
                }
        }
        return false;
    }

    changeBranchLengthRatio(change) {
        if ((this.inputBranchLengthRatio + change) <= 0)
            return

        this.inputBranchLengthRatio += change
        this.update(this.root)
    }

    changeBranchHeightRatio(change) {
        if ((this.inputBranchHeightRatio + change) <= 0)
            return

        this.inputBranchHeightRatio += change
        this.tree = this.tree.nodeSize([BRANCH_HEIGHT * this.inputBranchHeightRatio, 0])
        this.update(this.root)
    }

    setNodeFillColor(d) {
        if (!isNaN(d.state))
            return isCollapsible(d) ? this.colorPalette[d.state * 2 + 1] : this.colorPalette[d.state * 2];
        else
            return isCollapsible(d) ? this.colorPalette[1] : this.colorPalette[0];
    }

    setNodeStrokeColor(d) {
        return !isNaN(d.state) ? this.colorPalette[d.state * 2] : this.colorPalette[0];
    }

    setLinkStrokeColor(d) {
        return !isNaN(d.target.state) ? this.colorPalette[d.target.state * 2] : this.colorPalette[0];
    }

	setNodeRadius(d) {
        if (!isLeaf(d))
            return NODE_RADIUS;
        else
            return LEAF_RADIUS;
    }

	setNodeTextDistance(node) {
        if (!isLeaf(node)) {
            let distanceFromLeaf = this.treeMaxDepth * this.branchLengthRatio - node.y
            return distanceFromLeaf + TEXT_DISTANCE_FROM_LEAF;
        }
        else
            return TEXT_DISTANCE_FROM_LEAF;
    }
}

/**
 * Auxiliary functions that are not directly related to the tree visualization state
 */

// Function to center visualization on node
function centerSvgOnNode(source, svgWidth, svgHeight, zoomListener, svg) {
    let scale = zoomListener.scale();
    let x = source.y0 * scale;
    let y = source.x0 * scale;
    let translateX = svgWidth / 2 - x;
    let translateY = svgHeight / 2 - y;
    svg.transition()
        .duration(CENTER_TRANSITION_DURATION)
        .attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
    zoomListener.scale(scale);
    zoomListener.translate([translateX, translateY]);
}

// Function to center tree on svg
function centerTreeOnSvg(root, treeWidth, svgWidth, svgHeight, zoomListener, svg) {
    let scale = zoomListener.scale();
    let y = root.x0 * scale;
    let translateX = svgWidth / 2 - treeWidth / 2 * scale
    let translateY = svgHeight / 2 - y
    svg.transition()
        .duration(CENTER_TRANSITION_DURATION)
        .attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
    zoomListener.scale(scale);
    zoomListener.translate([translateX, translateY]);
}

function collapseOrExpand(d) {
    if (d.children) {
        d._children = d.children
        d._children.forEach(collapse)
        d.children = null
    } else if (d._children) {
        d.children = d._children
        d.children.forEach(expand)
        d._children = null
    }
    return d;
}

function collapseOrSingleExpand(d) {
    if (d.children) {
        d._children = d.children
        d._children.forEach(collapse)
        d.children = null
    } else if (d._children) {
        d.children = d._children
        d._children = null
    }
    return d;
}

function collapse(d) {
    if (d.children) {
        d._children = d.children;
        d._children.forEach(collapse);
        d.children = null;
    }
}

function expand(d) {
    if (d._children) {
        d.children = d._children;
        d.children.forEach(expand);
        d._children = null;
    }
}

function singleLevelExpand(d) {
    if (d._children) {
        d.children = d._children;
        d._children = null;
    }
}

function isCollapsible(node) {
    return node._children && (node._children.length > 0)
}

function isLeaf(node) {
    return !((node.children && node.children.length > 0) || (node._children && node._children.length > 0))
}